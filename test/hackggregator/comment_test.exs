defmodule Hackggregator.CommentTest do
  use ExUnit.Case
  import Mox

  setup :verify_on_exit!

  setup_all do
    {:ok, _pid} = Mox.Application.start(%{}, %{})
    :ok
  end

  test "fetch commment from the API" do
    Hackggregator.Client.Mock
    |> expect(:get_item, 4, fn id -> get_item_data(id) end)

    assert Hackggregator.Comment.get(1) == %Hackggregator.Comment{
             by: "someone",
             id: 1,
             text: "Some parent text",
             time: 1_314_211_127,
             deleted: false,
             dead: false,
             children: [
               %Hackggregator.Comment{
                 by: "someone2",
                 id: 2,
                 text: "Some child text",
                 time: 1_314_211_000,
                 deleted: false,
                 dead: false,
                 children: [
                   %Hackggregator.Comment{
                     by: "someone_nested",
                     id: 4,
                     text: "Some nested text",
                     time: 1_314_222_000,
                     deleted: false,
                     dead: false,
                     children: []
                   }
                 ]
               },
               %Hackggregator.Comment{
                 id: 3,
                 time: 1_314_211_999,
                 deleted: true,
                 dead: false,
                 children: []
               }
             ]
           }
  end

  defp get_item_data(1) do
    %{
      by: "someone",
      id: 1,
      kids: [2, 3],
      parent: 0,
      text: "Some parent text",
      time: 1_314_211_127,
      type: "comment"
    }
  end

  defp get_item_data(2) do
    %{
      by: "someone2",
      id: 2,
      kids: [4],
      parent: 0,
      text: "Some child text",
      time: 1_314_211_000,
      type: "comment"
    }
  end

  defp get_item_data(3) do
    %{
      id: 3,
      kids: [],
      deleted: true,
      parent: 0,
      time: 1_314_211_999,
      type: "comment"
    }
  end

  defp get_item_data(4) do
    %{
      by: "someone_nested",
      id: 4,
      kids: [],
      parent: 0,
      text: "Some nested text",
      time: 1_314_222_000,
      type: "comment"
    }
  end
end
