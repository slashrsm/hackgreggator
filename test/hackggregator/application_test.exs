defmodule Hackggregator.ApplicationTest do
  use ExUnit.Case

  test "check if all processes are ran initially" do
    {:ok, _} = Application.ensure_all_started(:cowboy)
    {:ok, pid} = Hackggregator.Application.start(nil, nil)

    children_count = Supervisor.count_children(pid)
    assert children_count.specs == 3
    assert children_count.active == 3
    assert children_count.supervisors == 1

    children = Supervisor.which_children(pid)
    assert 1 == Enum.count(Enum.filter(children, fn {id, _, _, _} -> id == Storage end))
    assert 1 == Enum.count(Enum.filter(children, fn {id, _, _, _} -> id == Fetcher end))
    assert 1 == Enum.count(Enum.filter(children, fn {id, _, _, _} -> id == Fetcher end))

    assert 1 ==
             Enum.count(
               Enum.filter(children, fn {id, _, _, _} ->
                 id == {:ranch_listener_sup, Hackggregator.API.Http.HTTP}
               end)
             )
  end
end
