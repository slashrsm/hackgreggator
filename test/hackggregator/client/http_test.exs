defmodule Hackggregator.Client.HTTPTest do
  use ExUnit.Case
  @moduletag :hacker_news_api

  setup_all do
    {:ok, _pid} = HTTPoison.start()
    :ok
  end

  test "a story is fetched" do
    assert Hackggregator.Client.HTTP.get_item(8863) == %{
             by: "dhouston",
             descendants: 71,
             id: 8863,
             kids: [
               9224,
               8917,
               8952,
               8884,
               8887,
               8869,
               8958,
               8940,
               8908,
               9005,
               8873,
               9671,
               9067,
               9055,
               8865,
               8881,
               8872,
               8955,
               10_403,
               8903,
               8928,
               9125,
               8998,
               8901,
               8902,
               8907,
               8894,
               8870,
               8878,
               8980,
               8934,
               8943,
               8876
             ],
             score: 104,
             time: 1_175_714_200,
             title: "My YC app: Dropbox - Throw away your USB drive",
             type: "story",
             url: "http://www.getdropbox.com/u/2/screencast.html"
           }
  end

  test "the top stories list is fetched" do
    top_stories = Hackggregator.Client.HTTP.top_stories()

    assert is_list(top_stories) == true
    Enum.each(top_stories, &is_integer/1)
  end
end
