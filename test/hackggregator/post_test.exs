defmodule Hackggregator.PostTest do
  use ExUnit.Case
  import Mox

  setup :verify_on_exit!

  setup_all do
    {:ok, _pid} = Mox.Application.start(%{}, %{})
    :ok
  end

  test "a story is created" do
    Hackggregator.Client.Mock
    |> expect(:get_item, 2, fn id -> get_item_data(id) end)

    assert Hackggregator.Post.get(1) == %Hackggregator.Post{
             id: 1,
             deleted: false,
             type: :story,
             by: "dhouston",
             time: 1_175_714_200,
             dead: false,
             url: "http://www.getdropbox.com/u/2/screencast.html",
             score: 111,
             title: "My YC app: Dropbox - Throw away your USB drive",
             descendants: 1,
             children: [
               %Hackggregator.Comment{
                 by: "someone2",
                 id: 2,
                 text: "Some child text",
                 time: 1_314_211_000,
                 deleted: false,
                 dead: false,
                 children: []
               }
             ]
           }
  end

  test "a job is created" do
    Hackggregator.Client.Mock
    |> expect(:get_item, 1, fn id -> get_item_data(id) end)

    assert Hackggregator.Post.get(3) == %Hackggregator.Post{
             id: 3,
             deleted: false,
             type: :job,
             by: "justin",
             time: 1_210_981_217,
             text:
               "Justin.tv is the biggest live video site online. We serve hundreds of thousands of video streams a day, and have supported up to 50k live concurrent viewers. Our site is growing every week, and we just added a 10 gbps line to our colo. Our unique visitors are up 900% since January.<p>There are a lot of pieces that fit together to make Justin.tv work: our video cluster, IRC server, our web app, and our monitoring and search services, to name a few. A lot of our website is dependent on Flash, and we're looking for talented Flash Engineers who know AS2 and AS3 very well who want to be leaders in the development of our Flash.<p>Responsibilities<p><pre><code>    * Contribute to product design and implementation discussions\n    * Implement projects from the idea phase to production\n    * Test and iterate code before and after production release \n</code></pre>\nQualifications<p><pre><code>    * You should know AS2, AS3, and maybe a little be of Flex.\n    * Experience building web applications.\n    * A strong desire to work on website with passionate users and ideas for how to improve it.\n    * Experience hacking video streams, python, Twisted or rails all a plus.\n</code></pre>\nWhile we're growing rapidly, Justin.tv is still a small, technology focused company, built by hackers for hackers. Seven of our ten person team are engineers or designers. We believe in rapid development, and push out new code releases every week. We're based in a beautiful office in the SOMA district of SF, one block from the caltrain station. If you want a fun job hacking on code that will touch a lot of people, JTV is for you.<p>Note: You must be physically present in SF to work for JTV. Completing the technical problem at <a href=\"http://www.justin.tv/problems/bml\" rel=\"nofollow\">http://www.justin.tv/problems/bml</a> will go a long way with us. Cheers!",
             dead: false,
             children: [],
             score: 6,
             title: "Justin.tv is looking for a Lead Flash Engineer!"
           }
  end

  test "a poll is created" do
    Hackggregator.Client.Mock
    |> expect(:get_item, 3, fn id -> get_item_data(id) end)

    assert Hackggregator.Post.get(4) == %Hackggregator.Post{
             id: 4,
             deleted: false,
             type: :poll,
             by: "pg",
             time: 1_204_403_652,
             dead: false,
             children: [
               %Hackggregator.Comment{
                 by: "someone2",
                 id: 2,
                 text: "Some child text",
                 time: 1_314_211_000,
                 deleted: false,
                 dead: false,
                 children: []
               }
             ],
             score: 46,
             title: "Poll: What would happen if News.YC had explicit support for polls?",
             poll_options: [
               %Hackggregator.PollOpt{
                 by: "pg",
                 id: 1,
                 score: 335,
                 text: "Yes, ban them; I'm tired of seeing Valleywag stories on News.YC.",
                 time: 1_207_886_576,
                 deleted: false,
                 dead: false
               }
             ],
             descendants: 54
           }
  end

  defp get_item_data(1) do
    %{
      by: "dhouston",
      descendants: 1,
      id: 1,
      kids: [2],
      score: 111,
      time: 1_175_714_200,
      title: "My YC app: Dropbox - Throw away your USB drive",
      type: "story",
      url: "http://www.getdropbox.com/u/2/screencast.html"
    }
  end

  defp get_item_data(2) do
    %{
      by: "someone2",
      id: 2,
      kids: [],
      parent: 0,
      text: "Some child text",
      time: 1_314_211_000,
      type: "comment"
    }
  end

  defp get_item_data(3) do
    %{
      by: "justin",
      id: 3,
      score: 6,
      text:
        "Justin.tv is the biggest live video site online. We serve hundreds of thousands of video streams a day, and have supported up to 50k live concurrent viewers. Our site is growing every week, and we just added a 10 gbps line to our colo. Our unique visitors are up 900% since January.<p>There are a lot of pieces that fit together to make Justin.tv work: our video cluster, IRC server, our web app, and our monitoring and search services, to name a few. A lot of our website is dependent on Flash, and we're looking for talented Flash Engineers who know AS2 and AS3 very well who want to be leaders in the development of our Flash.<p>Responsibilities<p><pre><code>    * Contribute to product design and implementation discussions\n    * Implement projects from the idea phase to production\n    * Test and iterate code before and after production release \n</code></pre>\nQualifications<p><pre><code>    * You should know AS2, AS3, and maybe a little be of Flex.\n    * Experience building web applications.\n    * A strong desire to work on website with passionate users and ideas for how to improve it.\n    * Experience hacking video streams, python, Twisted or rails all a plus.\n</code></pre>\nWhile we're growing rapidly, Justin.tv is still a small, technology focused company, built by hackers for hackers. Seven of our ten person team are engineers or designers. We believe in rapid development, and push out new code releases every week. We're based in a beautiful office in the SOMA district of SF, one block from the caltrain station. If you want a fun job hacking on code that will touch a lot of people, JTV is for you.<p>Note: You must be physically present in SF to work for JTV. Completing the technical problem at <a href=\"http://www.justin.tv/problems/bml\" rel=\"nofollow\">http://www.justin.tv/problems/bml</a> will go a long way with us. Cheers!",
      time: 1_210_981_217,
      title: "Justin.tv is looking for a Lead Flash Engineer!",
      type: "job",
      url: ""
    }
  end

  defp get_item_data(4) do
    %{
      by: "pg",
      descendants: 54,
      id: 4,
      kids: [2],
      score: 46,
      text: "",
      time: 1_204_403_652,
      title: "Poll: What would happen if News.YC had explicit support for polls?",
      parts: [5],
      type: "poll"
    }
  end

  defp get_item_data(5) do
    %{
      by: "pg",
      id: 1,
      poll: 160_704,
      score: 335,
      text: "Yes, ban them; I'm tired of seeing Valleywag stories on News.YC.",
      time: 1_207_886_576,
      type: "pollopt"
    }
  end
end
