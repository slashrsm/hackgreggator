defmodule Hackggregator.FethcerTest do
  use ExUnit.Case, async: false
  import ExUnit.CaptureLog
  import Mox

  setup :verify_on_exit!

  setup_all do
    {:ok, _pid} = Mox.Application.start(%{}, %{})
    {:ok, _pid} = Hackggregator.Storage.start_link()
    :ok
  end

  test "posts are fetched and saved" do
    Hackggregator.Client.Mock
    |> expect(:top_stories, 1, fn -> [1] end)
    |> expect(:get_item, 2, fn id -> get_item_data(id) end)

    assert capture_log(fn -> Hackggregator.Fetcher.handle_info(:work, %{period: 300}) end) =~
             "Fetched new stories from the API."

    post = %Hackggregator.Post{
      by: "dhouston",
      descendants: 1,
      id: 1,
      score: 111,
      time: 1_175_714_200,
      title: "My YC app: Dropbox - Throw away your USB drive",
      type: :story,
      url: "http://www.getdropbox.com/u/2/screencast.html",
      deleted: false,
      dead: false,
      children: [
        %Hackggregator.Comment{
          by: "someone2",
          id: 2,
          text: "Some child text",
          time: 1_314_211_000,
          deleted: false,
          dead: false,
          children: []
        }
      ]
    }

    assert Hackggregator.Storage.get_topstories() == [post]
    assert Hackggregator.Storage.get_post(1) == post
  end

  test "work reschduled if API not accessible" do
    Hackggregator.Client.Mock
    |> expect(:top_stories, 1, fn -> raise MatchError end)

    assert capture_log(fn -> Hackggregator.Fetcher.handle_info(:work, %{period: 300}) end) =~
             "Unable to fetch stories from the API. Retrying in 1 minute."
  end

  defp get_item_data(1) do
    %{
      by: "dhouston",
      descendants: 1,
      id: 1,
      kids: [2],
      score: 111,
      time: 1_175_714_200,
      title: "My YC app: Dropbox - Throw away your USB drive",
      type: "story",
      url: "http://www.getdropbox.com/u/2/screencast.html"
    }
  end

  defp get_item_data(2) do
    %{
      by: "someone2",
      id: 2,
      kids: [],
      parent: 0,
      text: "Some child text",
      time: 1_314_211_000,
      type: "comment"
    }
  end
end
