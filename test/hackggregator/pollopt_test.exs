defmodule Hackggregator.PollOptTest do
  use ExUnit.Case
  import Mox

  setup :verify_on_exit!

  setup_all do
    {:ok, _pid} = Mox.Application.start(%{}, %{})
    :ok
  end

  test "fetch poll option from the API" do
    Hackggregator.Client.Mock
    |> expect(:get_item, 1, fn id -> get_item_data(id) end)

    assert Hackggregator.PollOpt.get(1) == %Hackggregator.PollOpt{
             by: "pg",
             id: 1,
             score: 335,
             text: "Yes, ban them; I'm tired of seeing Valleywag stories on News.YC.",
             time: 1_207_886_576,
             deleted: false,
             dead: false
           }
  end

  defp get_item_data(1) do
    %{
      by: "pg",
      id: 1,
      poll: 160_704,
      score: 335,
      text: "Yes, ban them; I'm tired of seeing Valleywag stories on News.YC.",
      time: 1_207_886_576,
      type: "pollopt"
    }
  end
end
