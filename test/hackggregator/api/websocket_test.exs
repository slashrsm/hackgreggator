defmodule Hackggregator.API.WebsocketTest do
  use ExUnit.Case, async: false
  import ExUnit.CaptureLog

  setup do
    {:ok, _pid} = Hackggregator.Storage.start_link()
    :ok
  end

  test "handler init" do
    assert {:cowboy_websocket, %{some: "req"}, %{some: "state"}} ==
             Hackggregator.API.Websocket.init(%{some: "req"}, %{some: "state"})
  end

  test "subscribed to notifications on init" do
    assert {:ok, %{some: "state"}} == Hackggregator.API.Websocket.websocket_init(%{some: "state"})

    post = %Hackggregator.Post{
      id: 1,
      deleted: false,
      type: :story,
      by: "dhouston",
      time: 1_175_714_200,
      dead: false,
      url: "http://www.getdropbox.com/u/2/screencast.html",
      score: 111,
      title: "My YC app: Dropbox - Throw away your USB drive",
      descendants: 1,
      children: [
        %Hackggregator.Comment{
          by: "someone2",
          id: 2,
          text: "Some child text",
          time: 1_314_211_000,
          deleted: false,
          dead: false,
          children: []
        }
      ]
    }

    assert capture_log(fn -> Hackggregator.Storage.put_new_posts([post], [post.id]) end) =~
             "Sent new top stories to 1 subscribers."

    assert_receive :new_posts
  end

  test "sockets recieve top list" do
    assert {:ok, %{some: "state"}} ==
             Hackggregator.API.Websocket.websocket_info(:new_posts, %{some: "state"})

    post = %Hackggregator.Post{
      id: 1,
      deleted: false,
      type: :story,
      by: "dhouston",
      time: 1_175_714_200,
      dead: false,
      url: "http://www.getdropbox.com/u/2/screencast.html",
      score: 111,
      title: "My YC app: Dropbox - Throw away your USB drive",
      descendants: 1,
      children: [
        %Hackggregator.Comment{
          by: "someone2",
          id: 2,
          text: "Some child text",
          time: 1_314_211_000,
          deleted: false,
          dead: false,
          children: []
        }
      ]
    }

    Hackggregator.Storage.put_topstories([post])
    Hackggregator.Storage.put_post(post)

    assert {:reply,
            {:text,
             "[{\"by\":\"dhouston\",\"children\":[{\"by\":\"someone2\",\"children\":[],\"dead\":false,\"deleted\":false,\"id\":2,\"text\":\"Some child text\",\"time\":1314211000}],\"dead\":false,\"deleted\":false,\"descendants\":1,\"id\":1,\"poll_options\":null,\"score\":111,\"text\":null,\"time\":1175714200,\"title\":\"My YC app: Dropbox - Throw away your USB drive\",\"type\":\"story\",\"url\":\"http://www.getdropbox.com/u/2/screencast.html\"}]"},
            %{some: "state"}} ==
             Hackggregator.API.Websocket.websocket_info(:new_posts, %{some: "state"})
  end

  test "messages from client are ignored" do
    assert {:ok, %{some: "state"}} ==
             Hackggregator.API.Websocket.websocket_handle(:foo, %{some: "state"})
  end

  test "subscription is closed when connection is closewd" do
    assert capture_log(fn -> Hackggregator.API.Websocket.websocket_init(%{some: "state"}) end) =~
             "New subscription."

    assert capture_log(fn -> Hackggregator.API.Websocket.terminate(%{}, %{}, %{}) end) =~
             "Removed subscription."
  end
end
