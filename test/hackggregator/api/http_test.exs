defmodule Hackggregator.API.HttpTest do
  use ExUnit.Case, async: false
  use Plug.Test

  @opts Hackggregator.API.Http.init([])

  setup do
    {:ok, _pid} = Hackggregator.Storage.start_link()
    :ok
  end

  test "returns 404 for an unknown route" do
    conn =
      :get
      |> conn("/foo", "")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.status == 404
    assert conn.resp_body == "Not found."
  end

  test "returns 404 for non-existing top stories list" do
    conn =
      :get
      |> conn("/topstories/1")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.status == 404
    assert conn.resp_body == "Not found."
  end

  test "returns top stories list" do
    post = %Hackggregator.Post{
      id: 1,
      deleted: false,
      type: :story,
      by: "dhouston",
      time: 1_175_714_200,
      dead: false,
      url: "http://www.getdropbox.com/u/2/screencast.html",
      score: 111,
      title: "My YC app: Dropbox - Throw away your USB drive",
      descendants: 1,
      children: [
        %Hackggregator.Comment{
          by: "someone2",
          id: 2,
          text: "Some child text",
          time: 1_314_211_000,
          deleted: false,
          dead: false,
          children: []
        }
      ]
    }

    Hackggregator.Storage.put_topstories([post])
    Hackggregator.Storage.put_post(post)

    conn =
      :get
      |> conn("/topstories/1")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == Jason.encode!([post])

    conn =
      :get
      |> conn("/topstories/2")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.status == 404
    assert conn.resp_body == "Not found."

    conn =
      :get
      |> conn("/topstories/0")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.status == 404
    assert conn.resp_body == "Not found."
  end

  test "returns 404 for non-existing post" do
    conn =
      :get
      |> conn("/post/1")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.resp_body == "Not found."
  end

  test "returns a story" do
    post = %Hackggregator.Post{
      id: 1,
      deleted: false,
      type: :story,
      by: "dhouston",
      time: 1_175_714_200,
      dead: false,
      url: "http://www.getdropbox.com/u/2/screencast.html",
      score: 111,
      title: "My YC app: Dropbox - Throw away your USB drive",
      descendants: 1,
      children: [
        %Hackggregator.Comment{
          by: "someone2",
          id: 2,
          text: "Some child text",
          time: 1_314_211_000,
          deleted: false,
          dead: false,
          children: []
        }
      ]
    }

    Hackggregator.Storage.put_post(post)

    conn =
      :get
      |> conn("/post/1")
      |> Hackggregator.API.Http.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == Jason.encode!(post)
  end
end
