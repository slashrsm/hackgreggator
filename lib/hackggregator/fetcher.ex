defmodule Hackggregator.Fetcher do
  @moduledoc """
  Fetches posts from the Hacker News API.
  """

  use GenServer
  require Logger

  @doc """
  Starts a Hackggregator.Fetcher GenServer process.

  ## Arguments
    - `period`: Fetching frequency in seconds.
  """
  @spec start_link(non_neg_integer) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(period \\ 300) do
    GenServer.start_link(__MODULE__, %{period: period})
  end

  @spec init(map) :: {:ok, map}
  def init(state) do
    schedule_work(1)
    {:ok, state}
  end

  @doc """
  Fetches new posts from the API.
  """
  @spec handle_info(:work, map) :: {:noreply, map}
  def handle_info(:work, state) do
    try do
      client = Application.get_env(:hackggregator, :client, Hackggregator.Client.HTTP)
      top_stories_ids = client.top_stories() |> Enum.slice(0..49)

      top_stories =
        top_stories_ids
        |> Task.async_stream(
          fn id -> Hackggregator.Post.get(id) end,
          max_concurrency: 50,
          ordered: false,
          # This seems to be plenty to allow it to finish. These tasks spend most of the
          # time waiting for HTTP responses and that's why it needs to be a bit higher. Should
          # probably optimize the value for a production system but for a PoC like this it should
          # do just fine.
          timeout: Application.get_env(:hackggregator, :task_timeout, 120_000)
        )
        |> Enum.map(fn {:ok, post} -> post end)

      Logger.info("Fetched new stories from the API.")

      Hackggregator.Storage.put_new_posts(top_stories, top_stories_ids)

      schedule_work(state.period)
      {:noreply, state}
    rescue
      _ ->
        Logger.warn("Unable to fetch stories from the API. Retrying in 1 minute.")
        schedule_work(60)
        {:noreply, state}
    end
  end

  @spec schedule_work(non_neg_integer) :: reference
  defp schedule_work(period) do
    Process.send_after(self(), :work, period * 1000)
  end
end
