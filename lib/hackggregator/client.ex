defmodule Hackggregator.Client do
  @moduledoc """
  Defines behavior for the API client.
  """

  @doc """
  Gets an item from the API.
  """
  @callback get_item(non_neg_integer) :: map

  @doc """
  Gets list of top stories from the API.
  """
  @callback top_stories() :: [non_neg_integer]
end
