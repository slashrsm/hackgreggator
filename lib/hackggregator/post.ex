defmodule Hackggregator.Post do
  @moduledoc """
  Functions for Hacker News posts manipulation (story, job or poll).
  """
  import Hackggregator.Item

  @enforce_keys [:id, :deleted, :type, :by, :time, :dead, :title]

  @derive Jason.Encoder

  @typedoc """
  A Hacker News post (story, job or poll).
  """
  defstruct [
    :id,
    :type,
    :by,
    :time,
    :text,
    :url,
    :score,
    :title,
    :poll_options,
    :children,
    :descendants,
    deleted: false,
    dead: false
  ]

  @type t :: %__MODULE__{
          id: non_neg_integer,
          deleted: boolean,
          type: :story | :job | :poll,
          by: String.t(),
          time: non_neg_integer,
          text: String.t(),
          dead: boolean,
          children: [%Hackggregator.Comment{}],
          url: String.t(),
          score: non_neg_integer,
          title: String.t(),
          poll_options: [%Hackggregator.PollOpt{}],
          descendants: non_neg_integer
        }

  @doc """
  Gets a post from the API.
  """
  @spec get(non_neg_integer) :: %__MODULE__{}
  def get(comment_id) do
    client = Application.get_env(:hackggregator, :client, Hackggregator.Client.HTTP)
    client.get_item(comment_id) |> create()
  end

  @doc """
  Creates a post from a raw item fetched from the API.
  """
  @spec create(map) :: %__MODULE__{}
  def create(%{type: "story"} = raw_item) do
    %__MODULE__{
      id: raw_item.id,
      deleted: get_value(raw_item, :deleted, false),
      type: :story,
      by: raw_item.by,
      time: raw_item.time,
      text: get_value(raw_item, :text),
      dead: get_value(raw_item, :dead, false),
      children: get_children(raw_item),
      url: get_value(raw_item, :url),
      score: raw_item.score,
      title: get_value(raw_item, :title),
      descendants: get_value(raw_item, :descendants)
    }
  end

  def create(%{type: "job"} = raw_item) do
    %__MODULE__{
      id: raw_item.id,
      deleted: get_value(raw_item, :deleted, false),
      type: :job,
      by: raw_item.by,
      time: raw_item.time,
      text: get_value(raw_item, :text),
      dead: get_value(raw_item, :dead, false),
      children: get_children(raw_item),
      url: get_value(raw_item, :url),
      score: raw_item.score,
      title: get_value(raw_item, :title),
      descendants: get_value(raw_item, :descendants)
    }
  end

  def create(%{type: "poll"} = raw_item) do
    %__MODULE__{
      id: raw_item.id,
      deleted: get_value(raw_item, :deleted, false),
      type: :poll,
      by: raw_item.by,
      time: raw_item.time,
      text: get_value(raw_item, :text),
      dead: get_value(raw_item, :dead, false),
      children: get_children(raw_item),
      url: get_value(raw_item, :url),
      score: raw_item.score,
      title: get_value(raw_item, :title),
      poll_options: get_poll_options(raw_item),
      descendants: get_value(raw_item, :descendants)
    }
  end

  @spec get_children(map) :: [%Hackggregator.Comment{}]
  defp get_children(item) do
    get_value(item, :kids, [])
    |> Task.async_stream(
      fn id -> Hackggregator.Comment.get(id) end,
      max_concurrency: 50,
      # This seems to be plenty to allow it to finish. These tasks spend most of the
      # time waiting for HTTP responses and that's why it needs to be a bit higher. Should
      # probably optimize the value for a production system but for a PoC like this it should
      # do just fine.
      timeout: Application.get_env(:hackggregator, :task_timeout, 120_000)
    )
    |> Stream.map(fn {:ok, item} -> item end)
    |> Stream.filter(fn item -> item != nil end)
    |> Enum.to_list()
  end

  @spec get_poll_options(map) :: [%Hackggregator.PollOpt{}]
  defp get_poll_options(item) do
    get_value(item, :parts, [])
    |> Enum.map(&Hackggregator.PollOpt.get/1)
    |> Enum.filter(fn item -> item != nil end)
  end
end
