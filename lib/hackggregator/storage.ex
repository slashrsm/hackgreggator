defmodule Hackggregator.Storage do
  @moduledoc """
  Storage for the Hacker News posts.
  """

  use GenServer
  require Logger

  @doc """
  Starts a Hackggregator.Storage GenServer process.
  """
  @spec start_link :: :ignore | {:error, any} | {:ok, pid}
  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: Storage)
  end

  @doc """
  Stores a post into the memory storage.
  """
  @spec put_post(Hackggregator.Post.t()) :: :ok
  def put_post(%Hackggregator.Post{} = post) do
    GenServer.cast(Storage, {:put, post})
  end

  @doc """
  Stores entire result of a fetch from the API (all posts + top stories).
  """
  @spec put_new_posts([Hackggregator.Post.t()], [non_neg_integer]) :: :ok
  def put_new_posts(posts, topstories) do
    GenServer.cast(Storage, {:put_new_topstories, posts, topstories})
  end

  @doc """
  Stores a list of top stories into the memory storage.
  """
  @spec put_topstories([non_neg_integer, ...]) :: :ok
  def put_topstories([_ | _] = top) do
    GenServer.cast(Storage, {:put, top})
  end

  @doc """
  Subscribes a process to updates.
  """
  @spec subscribe(pid) :: :ok
  def subscribe(pid) do
    GenServer.cast(Storage, {:subscribe, pid})
  end

  @doc """
  Unsubscribes a process to updates.
  """
  @spec unsubscribe(pid) :: :ok
  def unsubscribe(pid) do
    GenServer.cast(Storage, {:unsubscribe, pid})
  end

  @doc """
  Gets a post from the memory storage.
  """
  @spec get_post(non_neg_integer) :: %Hackggregator.Post{} | false
  def get_post(id) do
    GenServer.call(Storage, {:get, id})
  end

  @doc """
  Gets a list of top stories from the memory storage.
  """
  @spec get_topstories() :: [non_neg_integer] | false
  def get_topstories() do
    GenServer.call(Storage, {:get, :topstories})
  end

  ###
  # GenServer API
  ###

  @spec init(map) :: {:ok, map}
  def init(state) do
    state =
      state
      |> Map.put(:ets_table, :ets.new(:hackgreggator_storage, [:set, :private]))
      |> Map.put(:subscribers, [])

    {:ok, state}
  end

  @doc """
  Inserts a post into the ETS table.
  """
  @spec handle_cast({:put, %Hackggregator.Post{}}, map) :: {:noreply, map}
  def handle_cast({:put, %Hackggregator.Post{} = item}, state) do
    :ets.insert(state.ets_table, {item.id, item})
    {:noreply, state}
  end

  @doc """
  Inserts top stories list into the ETS table.
  """
  @spec handle_cast({:put, [%Hackggregator.Post{}, ...]}, map) :: {:noreply, map}
  def handle_cast({:put, [_ | _] = item}, state) do
    :ets.insert(state.ets_table, {:topstories, item})
    {:noreply, state}
  end

  @doc """
  Adds a process to the subscribers list.
  """
  @spec handle_cast({:subscribe}, pid) :: {:noreply, map}
  def handle_cast({:subscribe, process}, state) do
    Logger.info("New subscription.")
    {:noreply, Map.put(state, :subscribers, [process | state.subscribers])}
  end

  @doc """
  Removes a process from the subscribers list.
  """
  @spec handle_cast({:unsubscribe}, pid) :: {:noreply, map}
  def handle_cast({:unsubscribe, process}, state) do
    Logger.info("Removed subscription.")
    {:noreply, Map.put(state, :subscribers, Enum.filter(state.subscribers, &(&1 != process)))}
  end

  @doc """
  Insertes entire result of a new fetch from the API (all posts + list of top stories).
  """
  @spec handle_cast(
          {:put_new_topstories, [%Hackggregator.Post{}, ...], [non_neg_integer, ...]},
          map
        ) :: {:noreply, map}
  def handle_cast({:put_new_topstories, posts, topstories_ids}, state) do
    # Save posts
    Enum.each(posts, fn post -> :ets.insert(state.ets_table, {post.id, post}) end)

    # Save list of top stories.
    topstories =
      topstories_ids
      |> Enum.map(fn id ->
        {:reply, post, _} = handle_call({:get, id}, nil, state)
        post
      end)

    :ets.insert(state.ets_table, {:topstories, topstories})
    Logger.info("List of new topstories saved.")

    # Garbage collection
    all_keys = get_all_keys([:ets.first(state.ets_table)], state.ets_table)
    to_delete = Enum.filter(all_keys, fn item -> !Enum.member?(topstories_ids, item) end)
    Enum.each(to_delete, fn id -> :ets.delete(state.ets_table, id) end)
    Logger.info("Deleted #{length(to_delete)} obsolete posts from the memory.")

    # Notify sockets.
    Enum.each(state.subscribers, &Process.send(&1, :new_posts, []))
    Logger.info("Sent new top stories to #{length(state.subscribers)} subscribers.")

    {:noreply, state}
  end

  @spec get_all_keys(list, any) :: [non_neg_integer]
  defp get_all_keys([:"$end_of_table" | list], _table) do
    list
  end

  defp get_all_keys([:topstories | list], table) do
    get_all_keys([:ets.next(table, :topstories) | list], table)
  end

  defp get_all_keys([previous | _] = list, table) do
    get_all_keys([:ets.next(table, previous) | list], table)
  end

  @doc """
  Gets an item from the ETS table.
  """
  @spec handle_call({:get, non_neg_integer | :topstories}, GenServer.from() | nil, map) ::
          {:reply, %Hackggregator.Post{}, map}
  def handle_call({:get, id}, _from, state) do
    item =
      case :ets.lookup(state[:ets_table], id) do
        [] -> false
        [{_id, item} | _] -> item
      end

    {:reply, item, state}
  end
end
