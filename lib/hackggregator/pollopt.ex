defmodule Hackggregator.PollOpt do
  @moduledoc """
  Functions for Hacker News poll options manipulation.
  """
  import Hackggregator.Item

  @enforce_keys [:id, :deleted, :time, :dead]

  @derive Jason.Encoder

  @typedoc """
  A Hacker News poll option.
  """
  defstruct [
    :id,
    :by,
    :time,
    :score,
    :text,
    deleted: false,
    dead: false
  ]

  @type t :: %__MODULE__{
          id: non_neg_integer,
          deleted: boolean,
          by: String.t(),
          time: non_neg_integer,
          text: String.t(),
          dead: boolean,
          score: non_neg_integer
        }

  @doc """
  Gets a poll option from the API.
  """
  @spec get(non_neg_integer) :: %__MODULE__{}
  def get(pollopt_id) do
    client = Application.get_env(:hackggregator, :client, Hackggregator.Client.HTTP)
    client.get_item(pollopt_id) |> create()
  end

  @doc """
  Creates a poll option from the raw item returned by the API.
  """
  @spec create(map) :: %__MODULE__{}
  def create(nil), do: nil

  def create(raw_item) do
    %__MODULE__{
      id: raw_item.id,
      deleted: get_value(raw_item, :deleted, false),
      by: get_value(raw_item, :by),
      time: raw_item.time,
      text: get_value(raw_item, :text),
      dead: get_value(raw_item, :dead, false),
      score: get_value(raw_item, :score, 0)
    }
  end
end
