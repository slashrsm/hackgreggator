defmodule Hackggregator.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      %{
        id: Storage,
        start: {Hackggregator.Storage, :start_link, []}
      },
      %{
        id: Fetcher,
        start: {Hackggregator.Fetcher, :start_link, []}
      },
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Hackggregator.API.Http,
        options: [
          port: Application.get_env(:hackggregator, :cowboy_port, 8080),
          dispatch: dispatch()
        ]
      )
    ]

    opts = [strategy: :one_for_one, name: Hackggregator.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {:_,
       [
         {"/socket", Hackggregator.API.Websocket, []},
         {:_, Plug.Cowboy.Handler, {Hackggregator.API.Http, []}}
       ]}
    ]
  end
end
