defmodule Hackggregator.Item do
  @moduledoc """
  Helper functions that are shared among all item types.
  """

  @doc """
  Gets value from a map assuming empty string an empty value.

  ## Examples

    iex> get_value(%{foo: "bar"}, :foo)
    "bar"
    iex> get_value(%{foo: ""}, :foo)
    nil
    iex> get_value(%{foo: ""}, :foo, "Foo is empty!")
    "Foo is empty!"

  """
  @spec get_value(map, atom, any) :: any
  def get_value(item, key, default \\ nil) do
    case Map.get(item, key, default) do
      "" -> default
      value -> value
    end
  end
end
