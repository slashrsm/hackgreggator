defmodule Hackggregator.Comment do
  @moduledoc """
  Functions for Hacker News comments manipulation.
  """
  import Hackggregator.Item

  @enforce_keys [:id, :deleted, :time, :dead]

  @derive Jason.Encoder

  @typedoc """
  A Hacker News comment.
  """
  defstruct [
    :id,
    :by,
    :time,
    :text,
    children: [],
    deleted: false,
    dead: false
  ]

  @type t :: %__MODULE__{
          id: non_neg_integer,
          deleted: boolean,
          by: String.t(),
          time: non_neg_integer,
          text: String.t(),
          dead: boolean,
          children: [%Hackggregator.Comment{}]
        }

  @doc """
  Gets a comment from the API.
  """
  @spec get(non_neg_integer) :: %__MODULE__{}
  def get(comment_id) do
    client = Application.get_env(:hackggregator, :client, Hackggregator.Client.HTTP)
    client.get_item(comment_id) |> create()
  end

  @doc """
  Creates a comment from the raw item returned by the API.
  """
  @spec create(map) :: %__MODULE__{}
  def create(nil), do: nil

  def create(raw_item) do
    %__MODULE__{
      id: raw_item.id,
      deleted: get_value(raw_item, :deleted, false),
      by: get_value(raw_item, :by),
      time: raw_item.time,
      text: get_value(raw_item, :text),
      dead: get_value(raw_item, :dead, false),
      children:
        get_value(raw_item, :kids, [])
        |> Task.async_stream(
          fn id -> get(id) end,
          max_concurrency: 50,
          # This seems to be plenty to allow it to finish. These tasks spend most of the
          # time waiting for HTTP responses and that's why it needs to be a bit higher. Should
          # probably optimize the value for a production system but for a PoC like this it should
          # do just fine.
          timeout: Application.get_env(:hackggregator, :task_timeout, 120_000)
        )
        |> Stream.map(fn {:ok, item} -> item end)
        |> Stream.filter(fn item -> item != nil end)
        |> Enum.to_list()
    }
  end
end
