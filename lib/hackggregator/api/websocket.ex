defmodule Hackggregator.API.Websocket do
  @moduledoc """
  WebSocket HTTP API implementation.
  """
  @behaviour :cowboy_websocket

  @doc """
  Initializes the handler.
  """
  def init(req, state) do
    {:cowboy_websocket, req, state}
  end

  @doc """
  Sends top stories and subscribes to notifications when the client opens the connection.
  """
  @spec websocket_init(map) :: {:ok, map} | {:reply, {:text, String.t()}, map}
  def websocket_init(state) do
    Hackggregator.Storage.subscribe(self())
    send_topstories_if_available(state)
  end

  @doc """
  Ignores any messages from the client.
  """
  @spec websocket_handle(any, map) :: {:ok, map}
  def websocket_handle(_, state) do
    {:ok, state}
  end

  @doc """
  Recieves messages from Hackgregator.Storage and sends topstories list to the client.
  """
  @spec websocket_info(:new_posts, map) :: {:ok, map} | {:reply, {:text, String.t()}, map}
  def websocket_info(:new_posts, state) do
    send_topstories_if_available(state)
  end

  @doc """
  Unsubscribes from notifications when the client closes the connection.
  """
  @spec terminate(any, any, any) :: :ok
  def terminate(_reason, _req, _state) do
    Hackggregator.Storage.unsubscribe(self())
    :ok
  end

  @spec send_topstories_if_available(map) :: {:ok, map} | {:reply, {:text, String.t()}, map}
  defp send_topstories_if_available(state) do
    case Hackggregator.Storage.get_topstories() do
      false -> {:ok, state}
      topstories -> {:reply, {:text, Jason.encode!(topstories)}, state}
    end
  end
end
