defmodule Hackggregator.API.Http do
  @moduledoc """
  JSON HTTP API implementation.
  """
  use Plug.Router

  @items_per_page 10

  plug(:match)
  plug(:dispatch)

  get "/topstories/:page" do
    {page, ""} = Integer.parse(page)

    if page < 1 do
      send_resp(conn, 404, "Not found.")
    else
      case get_topstories_page(page) do
        [] ->
          send_resp(conn, 404, "Not found.")

        topstories ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(topstories))
      end
    end
  end

  @spec get_topstories_page(non_neg_integer) :: [%Hackggregator.Post{}]
  defp get_topstories_page(page) do
    case Hackggregator.Storage.get_topstories() do
      false ->
        []

      topstories ->
        Enum.slice(topstories, (page - 1) * 10, @items_per_page)
    end
  end

  get "/post/:id" do
    {id, ""} = Integer.parse(id)

    case Hackggregator.Storage.get_post(id) do
      false ->
        send_resp(conn, 404, "Not found.")

      post ->
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, Jason.encode!(post))
    end
  end

  match _ do
    send_resp(conn, 404, "Not found.")
  end
end
