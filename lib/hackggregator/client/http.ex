defmodule Hackggregator.Client.HTTP do
  @moduledoc """
  API client implementation.
  """
  @behaviour Hackggregator.Client

  @api_url "https://hacker-news.firebaseio.com/v0"
  @topstories "/topstories.json"
  @item "/item/[item_id].json"

  @impl Hackggregator.Client
  @spec get_item(non_neg_integer) :: map
  def get_item(item_id) do
    {:ok, %HTTPoison.Response{body: item, status_code: 200}} =
      String.replace(@api_url <> @item, "[item_id]", Integer.to_string(item_id))
      |> HTTPoison.get()

    item |> Jason.decode!(keys: :atoms, strings: :copy)
  end

  @impl Hackggregator.Client
  @spec top_stories :: [non_neg_integer]
  def top_stories() do
    {:ok, %HTTPoison.Response{body: item, status_code: 200}} =
      HTTPoison.get(@api_url <> @topstories)

    item |> Jason.decode!(keys: :atoms, strings: :copy)
  end
end
