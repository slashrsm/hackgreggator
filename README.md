# Hackggregator

A Hacker news parser done as part of the Erlang Solutions code exercise.

## Fetcher

Story fetcher (`Hackggregator.Fetcher`) is a GenServer process that runs periodically, fetches top 50 stories from the API and stores them in an ETS table (running as `Hackggregator.Storage` GenServer process). After the application starts it will run for the first time and since the process takes a short period of time (approx. 20 seconds locally) there won't be any data available for the API endpoints during this period.

Fetcher will log an info message when the data becomes available:

```bash
15:31:16.649 [info]  Fetched new stories from the API.
15:31:16.655 [info]  List of new topstories saved.
15:31:16.655 [info]  Deleted 0 obsolete posts from the memory.
```

## HTTP API

Will run on port 8080 by default, but is configurable:

```elixir
config :hackggregator,
  cowboy_port: 80
```

It exposes two endpoints:

  - `/topstories/:page` - Returns top stories paginated 10 items per page (1-5).
  - `/post/:id` - Returns a specific post by the ID.

## Websocket API

Clients can connect to the websocket on `/socket`. They will receive the list of top stories when the connection is opened (if the data is available) and get the update every time new data is fetched from the Hacker News API. 

Any messages form the client are ignored.

## Tests

I strived towards 100% test coverage.

```
➜  hackggregator git:(master) ✗ mix coveralls --include hacker_news_api 
Including tags: [:hacker_news_api]

.......
17:42:35.352 [info]  New subscription.
..............

Finished in 0.9 seconds
1 doctest, 20 tests, 0 failures

Randomized with seed 234125
----------------
COV    FILE                                        LINES RELEVANT   MISSED
  0.0% lib/hackggregator.ex                            5        0        0
100.0% lib/hackggregator/api/http.ex                  58       16        0
100.0% lib/hackggregator/api/websocket.ex             52        7        0
100.0% lib/hackggregator/application.ex               41        3        0
  0.0% lib/hackggregator/client.ex                    15        0        0
100.0% lib/hackggregator/client/http.ex               29        4        0
100.0% lib/hackggregator/comment.ex                   73        8        0
100.0% lib/hackggregator/fetcher.ex                   67       14        0
100.0% lib/hackggregator/item.ex                      26        3        0
100.0% lib/hackggregator/pollopt.ex                   60        5        0
100.0% lib/hackggregator/post.ex                     134       22        0
100.0% lib/hackggregator/storage.ex                  185       33        0
[TOTAL] 100.0%
----------------
```

Run as usual 

```bash
mix test
```

There is a mock for API calls, but in order to test the actual client I created one test that actually calls the API. That test is excluded by default. In order to include it:

```bash
mix test --include hacker_news_api
```

Since multiple tests require `Hackaggregator.Storage` GenServer to be running I experienced collisions at some occasions (two tests trying to start it at the same time and one of them failing). This seemed to be a rare occasion so I decided not to investigate further.